import 'package:sparepart/appBar.dart';
import 'package:flutter/material.dart';
import 'package:sparepart/viewmodel.dart';

import 'drawer.dart';

class BerandaPage extends StatefulWidget {
  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(
          backgroundColor: Colors.amber[200],
          appBar: AllAppBar(),
          drawer: DrawerPage(),
          body: _gridFlexi()),
    );
  }

  List dataUser = new List();

  void getDataUser() {
    UserViewModel().getUsers().then((value) {
      setState(() {
        dataUser = value;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getDataUser();
  }

  Widget _gridFlexi() {
    return dataUser == null
        ? Center(child: CircularProgressIndicator())
        : new Container(
            child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2),
                itemCount: dataUser.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.all(8.0),
                    child: Center(
                        child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(10),
                          ),
                          color: Colors.amber,
                          border: Border.all(
                            color: Colors.black12,
                            width: 0.5,
                          )),
                      // padding: EdgeInsets.all(20),
                      child: Column(
                        children: [
                          Image.network(
                            dataUser[index]['gambar'] ??
                                "https://cdn1-production-images-kly.akamaized.net/SPsPZIwx8jHkiGuv9ybmowycmEw=/640x360/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/3128303/original/025341400_1589454782-parker-burchfield-tvG4WvjgsEY-unsplash.jpg",
                            cacheHeight: 115,
                            cacheWidth: 115,
                            fit: BoxFit.cover,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            dataUser[index]['nama'] ?? "Data Nama",
                            style: TextStyle(fontSize: 15),
                          ),
                          Text(
                            "Rp. " + dataUser[index]['harga'] ?? "Data Harga",
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    )),
                  );
                }),
          );
  }
}
