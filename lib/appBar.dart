import 'package:flutter/material.dart';

class AllAppBar extends AppBar {
  AllAppBar()
      : super(
            elevation: 0.25,
            backgroundColor: Color(0x44000D),
            flexibleSpace: _buildGojekAppBar());

  static Widget _buildGojekAppBar() {
    return Container(
      color: Color(0xcc44000D),
      child: new Center(
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
                padding: EdgeInsets.all(6.0),
                decoration: new BoxDecoration(
                    borderRadius:
                        new BorderRadius.all(new Radius.circular(100.0)),
                    color: Colors.transparent),
                alignment: Alignment.centerRight,
                child: Row(
                  children: [
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Sparepart",
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Color(0xff4ECCA3),
                          backgroundColor: Colors.transparent),
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
