import 'package:sparepart/appBar.dart';
import 'package:flutter/material.dart';
import 'drawer.dart';

class About extends StatefulWidget {
  @override
  _AboutState createState() => _AboutState();
}

class _AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AllAppBar(),
      drawer: DrawerPage(),
      body: Container(
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.only(top: 500),
          alignment: Alignment.bottomCenter,
          color: Color(0xffBBDFC8),
          child: Column(
            children: <Widget>[
              Text(
                "Copyright © Safarotul Indari, 18282019",
                style: TextStyle(color: Colors.black, fontSize: 17),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: Text(
                  "Sparepart",
                  style: TextStyle(color: Colors.black, fontSize: 17),
                ),
              )
            ],
          )),
    ));
  }
}
