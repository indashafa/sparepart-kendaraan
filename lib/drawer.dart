import 'package:flutter/material.dart';
import 'package:sparepart/about.dart';
import 'package:sparepart/inputData.dart';

import 'home.dart';

class DrawerPage extends StatefulWidget {
  @override
  _DrawerPageState createState() => _DrawerPageState();
}

class _DrawerPageState extends State<DrawerPage> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Color(0xee511845),
        // padding: EdgeInsets.all(15.0),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(30),
              margin: EdgeInsets.only(bottom: 20),
              width: MediaQuery.of(context).size.width,
              height: 200.0,
              color: Color(0xff44000D),
              alignment: Alignment.bottomLeft,
              child: Text(
                'SPAREPART',
                style: TextStyle(fontSize: 20.0, color: Color(0xffACFFAD)),
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.of(context)
                    .pushReplacement(new MaterialPageRoute(builder: (_) {
                  return new BerandaPage();
                }));
              },
              child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(0xffEA0599),
                  ),
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  height: 30.0,
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  child: Text('Home',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                      ))),
            ),
            SizedBox(
              height: 20,
            ),
            InkWell(
              onTap: () {
                Navigator.of(context)
                    .pushReplacement(new MaterialPageRoute(builder: (_) {
                  return new InputDataPage();
                }));
              },
              child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(0xffEA0599),
                  ),
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  height: 30.0,
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    'Input Data',
                    style: TextStyle(color: Colors.white, fontSize: 17),
                  )),
            ),
            SizedBox(
              height: 20,
            ),
            InkWell(
              onTap: () {
                Navigator.of(context)
                    .pushReplacement(new MaterialPageRoute(builder: (_) {
                  return new About();
                }));
              },
              child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(0xffEA0599),
                  ),
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  height: 30.0,
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    'About',
                    style: TextStyle(color: Colors.white, fontSize: 17),
                  )),
            )
          ],
        ),
      ),
    );
  }
}
