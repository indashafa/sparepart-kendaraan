import 'package:flutter/material.dart';

import 'appBar.dart';
import 'drawer.dart';
import 'jasonmodel.dart';
import 'viewmodel.dart';

class InputDataPage extends StatefulWidget {
  @override
  _InputDataPageState createState() => _InputDataPageState();
}

class _InputDataPageState extends State<InputDataPage> {
  TextEditingController _nama = new TextEditingController();
  TextEditingController _harga = new TextEditingController();
  TextEditingController _gambar = new TextEditingController();
  String msg = "", pesan = "";

  void _msg(msg) {
    pesan = msg;
  }

  @override
  void initState() {
    super.initState();
    _msg(msg);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AllAppBar(),
      drawer: DrawerPage(),
      body: _form(),
    ));
  }

  Widget _form() {
    return new Column(
      children: <Widget>[
        new ListTile(
          leading: const Icon(Icons.list_alt_sharp),
          title: new TextField(
            controller: _nama,
            decoration: new InputDecoration(
              hintText: "Nama Produk",
            ),
          ),
        ),
        new ListTile(
          leading: const Icon(Icons.monetization_on_outlined),
          title: new TextField(
            controller: _harga,
            decoration: new InputDecoration(
              hintText: "Harga Produk",
            ),
          ),
        ),
        new ListTile(
          leading: const Icon(Icons.photo_camera_back),
          title: new TextField(
            controller: _gambar,
            decoration: new InputDecoration(
              hintText: "Gambar Url produk",
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        new InkWell(
          onTap: () async {
            if (_gambar.text.isEmpty) {
              _msg('Isi Bagian Gambar');
            } else if (_nama.text.isEmpty) {
              _msg('Isi Bagian Nama');
            } else if (_harga.text.isEmpty) {
              _msg('Isi Bagian Harga');
            } else {
              UserpostModel commRequest = UserpostModel();
              commRequest.nama = _nama.text;
              commRequest.harga = _harga.text;
              commRequest.gambar = _gambar.text;

              UserViewModel()
                  .postUser(userpostModelToJson(commRequest))
                  .then((value) => print('success'));
              String nama = _nama.text;
              String harga = _harga.text;
              String gambar = _gambar.text;

              print(nama + harga + gambar);
            }
          },
          child: Container(
            width: 200,
            margin: EdgeInsets.only(top: 30),
            padding: EdgeInsets.symmetric(vertical: 15),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(90)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey.shade200,
                      offset: Offset(2, 4),
                      blurRadius: 5,
                      spreadRadius: 2)
                ],
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [Color(0xff4ECCA3), Colors.red[200]])),
            child: Text(
              'Tambah Produk',
              style: TextStyle(fontSize: 20, color: Colors.white),
            ),
          ),
        )
      ],
    );
  }
}
